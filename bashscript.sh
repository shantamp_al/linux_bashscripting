# Update VM Server
sudo apt-get Update

#Install nginx
sudo apt-get install nginx -y

# Start up nginx
sudo systemctl start nginx

# Replace nginx index.html to new index.html
sudo mv ~/shannon_website/index.html /var/www/html/index.nginx-debian.html

# Restart nginx
sudo systemctl restart nginx