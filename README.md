# Bash script to Install & configure NGINX on a ubuntu machine

### <u>Pre-requisites</u> 
* You will need a cloud host with a spun up ubuntu virtual machine
* SSH key connecting your cloud hosted remote machine to your local machine
* Bitbucket (connected to your machine using SSH key) to log your code.
* Text editor such as VS code, VIM or Nano

Below is a step by step guide on how to complete this task. 
____________________________________________________________

#### Step 1: Create a local REPO and push to remote REPO

```bash
# In your terminal create a directory 
$ mkdir <nameoffolder>

# Add two files
$ touch README.md bashscript.sh

# Create new folder to hold your index.html
$ mkdir <nameoffolder>

# Change directory
$ cd <nameoffolder>

# Add HTML file
$ touch index.html

> FYI - You can name your bashscript file any name you want as long as the extension is .sh. Same as your HTML file.

# Start a local repository using git
$ git init

# To check for changes 
$ git status

# If changes tracked add them by
$ git add .

# To apply changes 
¢ git commit -m "add meaningful short message"

```

#### Step 2: Pushing local REPO to remote REPO

1. Log into your *Bitbucket*
2. Create new repository - make sure you say **NO** to adding README.md and .gitignore, also set REPO to public.
3. Make sure you are connecting to Bitbucket using SSH

```bash

# To connect local REPO to remote REPO
$ git remote add origin git@bitbucket.org:<username>/<remotereponame>

# To check above command worked
$ git remote --v
> origin git@bitbucket.org:<user>/<remoterepo> (fetch)
> origin git@bitbucket.org:<user>/<remoterepo> (push)

# To push your local REPO to Bitbucket REPO
$ git push origin master

```

#### Step 3: Add content to your bashscript.sh and README.md

1. Open VS Code and open local REPO folder

2. Add information to your README.md - this should explain what the repo is for. Save using CMD+S

3. Starting writing your bashscript, for the commands required for this task [click here](https://bitbucket.org/shantamp_al/linux_bashscripting/src/master/bashscript.sh) - *P.S: You will need to manually check all of these commands to make sure they work before you run your script.*

4. In your index.html file start typing html and select html:5. This will give you the html template. For more information on how to write in html [click here](https://www.w3schools.com/html/html_styles.asp)

Once files all updated and you have CMD+S DON'T FORGET to follow above ` git commands ` to push to your remote REPO.

#### Step 4: Connect to Ubuntu machine and copy bashscript.sh and folder which holds your index.html file to your virtual machine.

<u> First make sure your SSH key is connected to your cloud host and terminal!</u>

```bash
 # To log into your ubuntu machine from local terminal 
 $ ssh -i ~/.ssh/<nameoffilewithkey> ubuntu@<ipaddressforvm>
 ```

- If prompted to ask if you want to continue yes/no/fingerprint, type `yes` into terminal and *enter* 

Now you are connected to your ubuntu machine from your local terminal. But before you can send the selected files, disconnect from your vm using command `exit`. Now you should be on your local terminal.

```bash

# To send a secure copy of bashscript to vm
$ scp -i ~/.ssh/<sshkeyfile> bashscript.sh ubuntu@<ipaddressforvm>:/home/ubuntu

# To send a copy of your directory which holds your html file
$ scp -i ~/.ssh/<sshkeyfile> -r <path/to/your/foler> ubuntu@<ipaddressofvm>:/home/ubuntu

```

- Now check the above commands work by connecting to the ubuntu machine.

```bash
# To log into your ubuntu machine from local terminal 
$ ssh -i ~/.ssh/<nameoffilewithkey> ubuntu@<ipaddressforvm>

# Check bashscript.sh  and folder was added
$ ls 
>bashscript.sh <nameoffolder>

```
#### Step 5: Give your bashscript permissions to run

```bash

# In Ubuntu machine give permission
$ chmod +x bashscript.sh

# Check permission given 
$ ll
> -rwxr-xr-x 1 Shannon  staff   296B 18 Jan 10:50 bashscript.sh

``` 

#### Step 6: Running your Bashscript

<u>You will need to run this command on your local terminal</u>

```bash

# Run bashscript on ubuntu vm
$ ssh -i ~/.ssh/<sshkeyfile> ubuntu@<ipaddressofvm> ./bashscript.sh

```

### You should have now successfully run your script and to check worked, open your virtual machine's IP on a web browser and you should see your personal index.html file's content.


